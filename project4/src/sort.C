// file sort.C  --  implementation of Sort class.

#include "sort.h"
#include "heapfile.h"
#include "system_defs.h"

#include <string.h>
#include <stdlib.h>

//added by me!
unsigned short sf_len, sf_offset;
AttrType sf_type;
TupleOrder sf_order;

int tupleCompare(const void *pRec1, const void *pRec2);
void int_to_charArray(int x, char* res, int& t, int base);

// Add you error message here
// static const char *sortErrMsgs[] {
// };
// static error_string_table sortTable(SORT, sortErrMsgs);

// constructor.
Sort::Sort ( char* inFile, char* outFile, int len_in, AttrType in[],
	     short str_sizes[], int fld_no, TupleOrder sort_order,
	     int amt_of_buf, Status& s )
{
	if(fld_no >= len_in) {
		s = FAIL;
		return;
	}

	rec_len = 0;
	for(int i=0; i<len_in; ++i) {
		rec_len += str_sizes[i];
	}

	outFileName = new char[NAMELEN];
	strcpy(outFileName, outFile);

	sf_type = in[fld_no];
	sf_len = str_sizes[fld_no];
	sf_offset = 0;
	for(int i=0; i<fld_no; ++i) {
		sf_offset += str_sizes[i];
	}
	sf_order = sort_order;

	s = OK;

	//phase one
	int pass_num = 1, HFfile_num;
	s = firstPass(inFile, amt_of_buf, HFfile_num);

	//following phases
	while(HFfile_num > 1) {
		++ pass_num;
		int tmp_HFfile_num = HFfile_num;
		s = followingPass(pass_num, tmp_HFfile_num, amt_of_buf, HFfile_num);
	}

	//transfer the final result into targeted output file
	char res_name[NAMELEN];

	RID garbage_rid;
	int garbage_recLen;
	
	makeHFname(res_name, pass_num, 0);
	HeapFile res_file(res_name, s);
	if(s != OK) {
		MINIBASE_FIRST_ERROR(FAIL, RES_FILE_ERROR);
		return;
	}
	Scan *res_scan = res_file.openScan(s);
	if(s != OK) {
		MINIBASE_FIRST_ERROR(FAIL, OPEN_SCAN_ERROR);
		return;
	}

	HeapFile out_file(outFile, s);
	if(s != OK) {
		MINIBASE_FIRST_ERROR(FAIL, OUT_FILE_ERROR);
		return;
	}

	char res_record[rec_len];
	while(1) {
		Status s2 = res_scan->getNext(garbage_rid, res_record, garbage_recLen);
		if(s2 == DONE) break;
		else if(s2 == OK) {
			out_file.insertRecord(res_record, rec_len, garbage_rid);
		}
		else {
			MINIBASE_FIRST_ERROR(FAIL, INSERT_RECORD_ERROR);
			s = s2;
			return;
		}
	}

	delete res_scan;
	res_file.deleteFile();
}

Status Sort::firstPass(char *inFile, int bufferNum, int& HFfile_num)
{
	Status status = OK;

	HeapFile in_file(inFile, status);
	if(status != OK) {
		MINIBASE_FIRST_ERROR(FAIL, OPEN_HEAPFILE_ERROR);
		return status;
	}
	Scan* in_scan = in_file.openScan(status);
	if(status != OK) {
		MINIBASE_FIRST_ERROR(FAIL, OPEN_SCAN_ERROR);
		return status;
	}

	int tot_record_num = in_file.getRecCnt();
	int tot_space = PAGESIZE * bufferNum;
	int record_num_per_run = tot_space / rec_len;

	HFfile_num = tot_record_num / record_num_per_run;
	if(tot_record_num % record_num_per_run) ++ HFfile_num;

	int garbage_recLen;
	char records[tot_space];
	RID garbage_rid;
	char new_HFfile_name[NAMELEN];
	HeapFile *new_file;

	for(int i=0; i<HFfile_num; ++i) {

		int num_of_record = 0;

		//open a new file
		makeHFname(new_HFfile_name, 1, i);
		new_file = new HeapFile(new_HFfile_name, status);
		if(status != OK) {
			MINIBASE_FIRST_ERROR(FAIL, NEW_HEAPFILE_ERROR);
			return status;
		}

		//read all records into memory
		for(int j=0; j<record_num_per_run; ++j) {

			Status s = in_scan->getNext(garbage_rid, records + j * rec_len, garbage_recLen);

			if(s == OK) ++num_of_record;
			else break;
		}

		//sort all the records
		qsort(records, num_of_record, rec_len, tupleCompare);
		
		//dump sorted records into HFfile
		for(int j=0; j<num_of_record; ++j) {
			new_file->insertRecord(records + j * rec_len, rec_len, garbage_rid);
		}

		delete new_file;
	}

	return status;
}

Status Sort::followingPass(int passNum, int oldHFnum, int bufferNum, int& newHFnum)
{
	Status status = OK;

	newHFnum = oldHFnum / (bufferNum - 1);
	if(oldHFnum % (bufferNum - 1)) ++newHFnum;

	Scan *scan[bufferNum - 1];
	HeapFile *file[bufferNum - 1];

	char name[NAMELEN];
	char outName[NAMELEN];

	HeapFile* out_file;

	for(int i=0; i<newHFnum; ++i) {

		int k = 0;

		for(int j=i*(bufferNum-1); j<min((i+1)*(bufferNum-1), oldHFnum); ++j) {

			makeHFname(name, passNum-1, j);

			file[k] = new HeapFile(name, status);
			if(status != OK) {
				MINIBASE_FIRST_ERROR(FAIL, OPEN_HEAPFILE_ERROR);
				return status;
			}

			scan[k] = file[k]->openScan(status);
			if(status != OK) {
				MINIBASE_FIRST_ERROR(FAIL, OPEN_SCAN_ERROR);
				return status;
			}

			++k;
		}

		makeHFname(outName, passNum, i);
		out_file = new HeapFile(outName, status);
		if(status != OK) {
			MINIBASE_FIRST_ERROR(FAIL, CREATE_HEAPFILE_ERROR);
			return status;
		}

		status = merge(scan, k, out_file);
		if(status != OK) {
			MINIBASE_FIRST_ERROR(FAIL, MEREG_ERROR);
			return status;
		}

		delete out_file;
		for(int j=0; j<k; ++j) {
			delete scan[j];
			file[j]->deleteFile();
			delete file[j];
		}
	}

	return status;
}

Status Sort::merge( Scan* scan[], int runNum, HeapFile* outHF )
{
	Status status = OK;

	RID garbage_rid;
	int garbage_recLen;

	//read the first of all runs
	char records[runNum][rec_len];
	bool next[runNum];

	for(int i=0; i<runNum; ++i) {

		Status s = scan[i]->getNext(garbage_rid, records[i], garbage_recLen);

		if(s == OK) next[i] = true;
		else if(s == DONE) next[i] = false;
		else {
			MINIBASE_FIRST_ERROR(FAIL, GET_NEXT_ERROR);
			return s;
		}
	}

	char *smallest_record;

	while(1) {

		//pick up the smallest one
		int runId = 0, smallest_runId = -1;
		while(runId < runNum) {
			if(next[runId] && (smallest_runId == -1 || tupleCompare(records[runId], smallest_record) < 0)) {
				smallest_record = records[runId];
				smallest_runId = runId;
			}
			++runId;
		}

		if(smallest_runId == -1) break;

		//insert the smallest record in all runs to the disk
		status = outHF->insertRecord(records[smallest_runId], rec_len, garbage_rid);
		if(status != OK) {
			MINIBASE_FIRST_ERROR(FAIL, INSERT_RECORD_ERROR);
			return status;
		}
		Status s = scan[smallest_runId]->getNext(garbage_rid, records[smallest_runId], garbage_recLen);
		if(s == DONE) next[smallest_runId] = false;
		else if(s != OK) {
			MINIBASE_FIRST_ERROR(FAIL, GET_NEXT_ERROR);
			return s;
		}
	}

	return status;
}

void Sort::makeHFname( char *name, int passNum, int HFnum )
{
	char tmp[NAMELEN];
	tmp[0] = '_';

	int len, len2;
	int_to_charArray(passNum, tmp+1, len, 256);
	tmp[len+1] = '_';
	int_to_charArray(HFnum, tmp+len+2, len2, 256);
	tmp[len+len2+2] = '\0';

	strcpy(name, tmp);
}

void int_to_charArray(int x, char* res, int& t, int base)
{
	char tmp[NAMELEN];

	t = 0;
	while(x>0) {
		tmp[t] = x%base;
		x /= base;
		++t;
	}

	if(t==0) {
		tmp[0] = 0;
		t = 1;
	}

	--t;

	for(int i=0; i<=t/2; ++i) {
		char t2 = tmp[i];
		tmp[i] = tmp[t-i];
		tmp[t-i] = t2;
	}

	++t;
	tmp[t] = '\0';

	strcpy(res, tmp);
}

int tupleCompare(const void *pRec1, const void *pRec2)
{
  int result;

  char *rec1 = (char *)pRec1;
  char *rec2 = (char *)pRec2;

  if (sf_type == attrInteger)
	result = *(int *)(rec1 + sf_offset) - *(int *)(rec2 + sf_offset);
  else
	result = (strncmp(&rec1[sf_offset], &rec2[sf_offset], sf_len));

  if (result < 0)
	if (sf_order == Ascending)
	  return -1;
	else
	  return 1;
  else
	if (result > 0)
	  if (sf_order == Ascending)
		return 1;
	  else
		return -1;
	else
	  return 0;
}
