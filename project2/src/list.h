#ifndef list_C
#define list_C

#include <cstdio>
#include <cstdlib>

struct node {

    int data;
    struct node *next;

    node(int data_n = -1, node * next_n = NULL) {
        data = data_n;
        next = next_n;
    }
};

class List {

private:

    node *head;
    node *tail;

public:

    List();
    ~List();

    int pop_front();
    int pop_end();
    void add_to_front(int x);
    void add_to_end(int x);
    int del(int x);
    void print();
};

#endif