#ifndef HashTable_C
#define HashTable_C

#include "buf.h"

#include <cstdio>
#include <cstdlib>

struct HashNode {

    PageId pageId;
    int frameNo;
    struct HashNode *next;

    HashNode(PageId pageId_n = -1, int frameNo_n = -1, HashNode* next_n = NULL) {
        pageId = pageId_n;
        frameNo = frameNo_n;
        next = next_n;
    }
};

class HashTable {

private:

    int a, b;
    HashNode bucket[HTSIZE]; 

    int hash(int x);

public:

    HashTable();
    ~HashTable();

     // return the frameNo if in buffer; otherwise, return -1
    int search(PageId pageId);
    // return -1 if not exist
    int del(PageId pageId);
    int add(PageId pageId, int frameNo);
    void print();
};


#endif
