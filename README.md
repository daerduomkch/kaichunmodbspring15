# README #

This is my codes for a project on course **Database System**, taught by [_Prof. Feifei Li_](http://www.cs.utah.edu/~lifeifei/) from the University of Utah. In this project, I was required to implement four core algorithms comprising the kernel of any database system.

# Contact:

Webpage: http://www.kaichun-mo.com