/*
 * btleaf_page.cc - implementation of class BTLeafPage
 *
 */

#include "btleaf_page.h"

const char* BTLeafErrorMsgs[] = {
};
static error_string_table btree_table(BTLEAFPAGE, BTLeafErrorMsgs);
   
Status BTLeafPage::insertRec(const void *key,
                              AttrType key_type,
                              RID dataRid,
                              RID& rid)
{
	int recLen;
	KeyDataEntry *recPtr = new KeyDataEntry;

	Datatype data;
	data.rid = dataRid;

	make_entry(recPtr, key_type, key, LEAF, data, &recLen);

	Status status = insertRecord(key_type, (char*) recPtr, recLen, rid);

	delete recPtr;
	return status;
}

Status BTLeafPage::get_data_rid(void *key,
                                AttrType key_type,
                                RID & dataRid)
{
	RID rid, tmp_rid;
	Keytype *cur_key = new Keytype;

	Status status = get_first(rid, cur_key, dataRid);
	while(status == OK) {
		if(keyCompare(key, cur_key, key_type) <= 0) break;

		tmp_rid = rid;
		status = get_next(tmp_rid, rid, cur_key, dataRid);
	}

	if(status == DONE) {
		RID *tmp_rid = new RID;
		tmp_rid->pageNo = -1; tmp_rid->slotNo = -1;
		dataRid = *tmp_rid;
		return FAIL;
	}
	if(keyCompare(key, cur_key, key_type) == 0) return OK;

	return FAIL;
}

Status BTLeafPage::get_rec_rid(void *key,
                                AttrType key_type,
                                RID & recRid)
{
	RID curRid, dataRid;
	Keytype *cur_key = new Keytype;

	Status status = get_first(recRid, cur_key, dataRid);
	while(status == OK) {

		if(keyCompare(key, cur_key, key_type) <= 0) break;

		curRid = recRid;
		status = get_next(curRid, recRid, cur_key, dataRid);
	}

	if(status == DONE) {
		RID *tmp_rid = new RID;
		tmp_rid->pageNo = -1; tmp_rid->slotNo = -1;
		recRid = *tmp_rid;
		return FAIL;
	}
	if(keyCompare(key, cur_key, key_type) == 0) return OK;

	return FAIL;
}

Status BTLeafPage::get_first (RID& rid,
                              void *key,
                              RID & dataRid)
{ 
	Status status = firstRecord(rid);
	if(status != OK) return status;

	return get_key_and_data(rid, key, dataRid);
}

Status BTLeafPage::get_next (RID curRid, RID& rid,
                             void *key,
                             RID & dataRid)
{
	Status status = nextRecord(curRid, rid);
	if(status != OK) return status;

	return get_key_and_data(rid, key, dataRid);
}

Status BTLeafPage::get_key_and_data(RID rid, void *key, RID & dataRid)
{
	KeyDataEntry *recPtr = new KeyDataEntry();
	int recLen;

	Status status = getRecord(rid, (char *)recPtr, recLen);

	if(status != OK) {
		delete recPtr;
		return status;
	}

	Datatype *targetdata = new Datatype;

	get_key_data(key, targetdata, recPtr, recLen, LEAF);

	dataRid = targetdata->rid;

	delete recPtr;
	delete targetdata;

	return OK;
}
