/*
 * btfile.h
 *
 * sample header file
 *
 */
 
#ifndef _BTREE_H
#define _BTREE_H

#include "btindex_page.h"
#include "btleaf_page.h"
#include "index.h"
#include "btreefilescan.h"
#include "bt.h"

// Define your error code for B+ tree here
// enum btErrCodes  {...}

enum btErrCodes {
	INDEX_FILE_NOT_EXIST, PIN_PAGE_FAILURE, TYPE_FAILURE, NEW_PAGE_FAILURE, ADD_FILE_ENTRY_FAILURE,
	REC_LARGER_THAN_PAGE_SIZE, FIND_TO_LEAF_PAGE_FAILURE, GET_REC_RID_FAILURE
};

struct BTreeHeaderPage {
	PageId root;
	AttrType key_type;
	int keysize;
	char filename[MAXFILENAME];
};

class BTreeFile: public IndexFile
{
  public:
    BTreeFile(Status& status, const char *filename);
    // an index with given filename should already exist,
    // this opens it.
    
    BTreeFile(Status& status, const char *filename, const AttrType keytype,  \
	      const int keysize);
    // if index exists, open it; else create it.
    
    ~BTreeFile();
    // closes index
    
    Status destroyFile();
    // destroy entire index file, including the header page and the file entry
    
    Status insert(const void *key, const RID rid);
    // insert <key,rid> into appropriate leaf page
    
    Status Delete(const void *key, const RID rid);
    // delete leaf entry <key,rid> from the appropriate leaf
    // you need not implement merging of pages when occupancy
    // falls below the minimum threshold (unless you want extra credit!)
    
    IndexFileScan *new_scan(const void *lo_key = NULL, const void *hi_key = NULL);	//low & high
    // create a scan with given keys
    // Cases:
    //      (1) lo_key = NULL, hi_key = NULL
    //              scan the whole index
    //      (2) lo_key = NULL, hi_key!= NULL
    //              range scan from min to the hi_key
    //      (3) lo_key!= NULL, hi_key = NULL
    //              range scan from the lo_key to max
    //      (4) lo_key!= NULL, hi_key!= NULL, lo_key = hi_key
    //              exact match ( might not unique)
    //      (5) lo_key!= NULL, hi_key!= NULL, lo_key < hi_key
    //              range scan from lo_key to hi_key

    int keysize();
    
    BTreeHeaderPage * headerPage = NULL;

    Status insert_to_leaf(Keytype *key, const RID rid, BTLeafPage *page,
    		PageId& new_pageId, Keytype*& mid_key);
    Status insert_to_index(Keytype *key, const RID rid, BTIndexPage *page,
    		PageId& new_pageId, Keytype*& mid_key);

    Status find_to_leaf_page(PageId pageId, const void *key, PageId& leaf_pageId);
    PageId leftmost_leaf_page(PageId pageId);

    void print();
    void print(PageId pageId);

  private:
    PageId headerPageId = -1;

    Status destroy_page(PageId pageId);
};

#endif
