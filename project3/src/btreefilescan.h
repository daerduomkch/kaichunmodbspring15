/*
 * btreefilescan.h
 *
 * sample header file
 *
 */
 
#ifndef _BTREEFILESCAN_H
#define _BTREEFILESCAN_H

#include "btfile.h"

// errors from this class should be defined in btfile.h

class BTreeFile;

class BTreeFileScan : public IndexFileScan {
public:
    friend class BTreeFile;

    // get the next record
    Status get_next(RID & rid, void* keyptr);

    // delete the record currently scanned
    Status delete_current();

    int keysize(); // size of the key

    BTreeFileScan(const void *lo_key, const void *hi_key, BTreeFile *btf);

    // destructor
    ~BTreeFileScan();

private:
    BTLeafPage *page;
    RID rec_rid, rid_entry;
    Keytype *max_key, *key_entry;

    BTreeFile *btf;

    bool first_next = true;

    Status next_record();
};

#endif
